<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Contracts\Validation\Validator;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ProfileController extends Controller
{
    protected $user;

    /**
     * Check if Login
     * @return void
     */
    public function __construct()
    {
        $this->user = new User;
        $this->middleware('auth');
    }

    /**
     * Show User Profile
     * @param $id User ID
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index($id)
    {
        return view('profile', [
            'user' => $id
        ]);
    }

    /**
     * Update Profile
     * @param Request $request
     * @param $id User ID
     * @return \Illuminate\Http\RedirectResponse
     */
    public function update(Request $request, $id)
    {
        $validatedData = $request->validate([
            'name' => 'required',
            'email' => 'required',
            'password' => 'required',
            'password_confirmation' => 'required',
            'password_current' => 'required',
        ]);
        $current_password = Auth::User()->password;
        if(Hash::check($validatedData['password_current'], $current_password)) {
            if ($validatedData['password'] == $validatedData['password_confirmation']) {
                $user = $this->user->findOrFail($id);
                $user->name = $validatedData['name'];
                $user->email = $validatedData['email'];
                $user->password = Hash::make($validatedData['password']);
                $user->save();
                return redirect()->action('ProfileController@index', ['id' => $id])->with('status', 3);
            } else {
                return redirect()->action('ProfileController@index', ['id' => $id])->with('status', 2);
            }
        }
        else {
            return redirect()->action('ProfileController@index', ['id' => $id])->with('status', 1);
        }
    }
}
