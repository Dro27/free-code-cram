<?php

namespace App\Http\Controllers;

use App\Manga;
use Illuminate\Http\Request;

class MangasController extends Controller
{
    /**
     * @var Manga
     */
    protected $manga;

    /**
     * MangasController constructor.
     */
    public function __construct()
    {
        $this->manga = new Manga;
    }

    /**
     * @param $title
     */
    public function index($title)
    {
        dd($title);
    }

    /**
     * @return mixed
     */
    public function latest()
    {
        return view('list', [
            'list' => $this->manga->orderBy('created_at', 'DESC')->paginate(5),
            'title' => 'Latest Updates'
        ]);
    }

    /**
     * @return mixed
     */
    public function hot()
    {
        return view('list', [
            'list' => $this->manga->orderBy('views', 'DESC')->paginate(5),
            'title' => 'Hot Mangas'
        ]);
    }
}
