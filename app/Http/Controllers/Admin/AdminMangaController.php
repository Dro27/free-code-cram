<?php

namespace App\Http\Controllers\Admin;

use App\Author;
use App\Manga;
use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class AdminMangaController extends Controller
{
    protected $manga;
    protected $authors;
    protected $user;

    /**
     * Check if Login
     * @return void
     */
    public function __construct()
    {
        $this->user = new User;
        $this->manga = new Manga;
        $this->authors = new Author;
        $this->middleware('auth');
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function index()
    {
        return view('admin/manga', [
            'list' => $this->getMangaList()
        ]);
    }

    /**
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function create()
    {
        return view('admin/create_manga', [
            'authors' => $this->getAuthorList()
        ]);
    }

    /**
     * @param $id
     * @return \Illuminate\Contracts\View\Factory|\Illuminate\View\View
     */
    public function edit($id)
    {
        $manga = $this->manga->where('id', '=', $id)->get();
        return view('admin/edit_manga', [
            'data' => $manga,
            'authors' => $this->getAuthorList()
        ]);
    }

    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function triggerCreate(Request $request)
    {
        $validatedData = request()->validate([
            'title' => 'required',
            'author' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $dir_name = strtolower(preg_replace('/\s+/', '_', $validatedData['title'])) . '-'. rand(1000,9999);;
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        $path = 'manga/' . $dir_name;

        request()->image->move(public_path($path), $imageName);

        $this->manga->title = $validatedData['title'];
        $this->manga->author_id = $validatedData['author'];
        $this->manga->genre = '1,2,3,4,5'; // @todo dynamic this shit
        $this->manga->rating = '7'; // @todo dynamic this shit
        $this->manga->status = 1;
        $this->manga->views = 0;
        $this->manga->thumbnail = $dir_name . '/'. $imageName;
        $this->manga->save();


        return redirect()->action('Admin\AdminMangaController@create',)->with('status', 1);
    }


    /**
     * @param Request $request
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function triggerUpdate(Request $request, $id)
    {
        $validatedData = request()->validate([
            'title' => 'required',
            'author' => 'required',
            'image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:2048',
        ]);

        $manga = $this->manga->findOrFail($id);
        $explode = explode('/', $manga->thumbnail);
        $imageName = time().'.'.request()->image->getClientOriginalExtension();
        $path = 'manga/' . $explode[0];

        request()->image->move(public_path($path), $imageName);


        $manga->title = $validatedData['title'];
        $manga->author_id = $validatedData['author'];
        $manga->thumbnail = $explode[0] . '/'. $imageName;
        $manga->save();


        return redirect()->action('Admin\AdminMangaController@edit', ['id' => $id])->with('status', 1);
    }


    /**
     * @param $id
     * @return \Illuminate\Http\RedirectResponse
     */
    public function delete(Request $request, $id)
    {
        $this->manga->where('id','=', $id)->delete();
        return redirect()->action('Admin\AdminMangaController@index', ['id' => $id])->with('is_deleted', 1);
    }

    /**
     * Get Manga with Pagination
     * @param int $skip
     * @param int $take
     * @return mixed
     */
    private function getMangaList()
    {
        return $this->manga->orderBy('id', 'ASC')->paginate(3);
    }

    /**
     * Get Authors
     * @return mixed
     */
    private function getAuthorList()
    {
        return $this->authors->get();
    }
}
