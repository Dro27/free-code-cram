<?php

namespace App\Http\Controllers\Admin;

use App\Http\Controllers\Controller;
use App\User;
use Illuminate\Http\Request;

class AdminDashboardController extends Controller
{
    protected $user;

    /**
     * Check if Login
     * @return void
     */
    public function __construct()
    {
        $this->user = new User;
        $this->middleware('auth');
    }

    public function index()
    {
        return view('admin/dashboard');
    }
}
