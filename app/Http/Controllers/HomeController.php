<?php

namespace App\Http\Controllers;

use App\Manga;
use Illuminate\Http\Request;

class HomeController extends Controller
{

    protected $manga;

    /**
     * Check if Login
     * @return void
     */
    public function __construct()
    {
        $this->manga = new Manga;
    }

    /**
     * Show homepage
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home', [
            'hotMangaUpdate' => $this->getHotMangaUpdates(),
            'highestRated' => $this->getTopHighestRated(),
            'latestUpdates' => $this->getLatestUpdates()
        ]);
    }

    /**
     * Get Hot Manga Update
     * @return mixed
     */
    private function getHotMangaUpdates()
    {
        return $this->manga->where('rating','>', 8.00)->skip(0)->take(5)->orderBy('created_at', 'DESC')->get();
    }

    /**
     * Get highest rated
     * @return mixed
     */
    private function getTopHighestRated()
    {
        return $this->manga->skip(0)->take(5)->orderBy('rating', 'DESC')->get();
    }

    /**
     * Get Latest Updates
     * @return mixed
     */
    private function getLatestUpdates()
    {
        return $this->manga->skip(0)->take(15)->orderBy('created_at', 'DESC')->get();
    }
}
