@extends('layouts.app')

@section('content')
    <div class="container py-4">
        <div id="featuredCarousel" class="carousel slide" data-ride="carousel">
            <ol class="carousel-indicators">
                <li data-target="#featuredCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#featuredCarousel" data-slide-to="1"></li>
                <li data-target="#featuredCarousel" data-slide-to="2"></li>
                <li data-target="#featuredCarousel" data-slide-to="3"></li>
                <li data-target="#featuredCarousel" data-slide-to="4"></li>
            </ol>
            <div class="carousel-inner">
                <div class="carousel-item active">
                    <a href="#">
                        <img class="d-block w-100" src="/images/featured/onepiece.jpg">
                    </a>
                </div>
                <div class="carousel-item">
                    <a href="#">
                        <img class="d-block w-100" src="/images/featured/boruto.jpg">
                    </a>
                </div>
                <div class="carousel-item">
                    <a href="#">
                        <img class="d-block w-100" src="/images/featured/onepunchman.jpg">
                    </a>
                </div>
                <div class="carousel-item">
                    <a href="#">
                        <img class="d-block w-100" src="/images/featured/neverland.jpg">
                    </a>
                </div>
                <div class="carousel-item">
                    <a href="#">
                        <img class="d-block w-100" src="/images/featured/chainsaw.jpg">
                    </a>
                </div>
            </div>
            <a class="carousel-control-prev" href="#featuredCarousel" role="button" data-slide="prev">
                <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                <span class="sr-only">Previous</span>
            </a>
            <a class="carousel-control-next" href="#featuredCarousel" role="button" data-slide="next">
                <span class="carousel-control-next-icon" aria-hidden="true"></span>
                <span class="sr-only">Next</span>
            </a>
        </div>
        <div class="section pt-md-5">
            <div class="row">
                <div class="col-6">
                    <h2 class="position-relative pl-3">
                        <span class="purple-indicator"></span>
                        Hot Updates
                    </h2>
                </div>
                <div class="col-6 text-right">
                    <a href="#" class="text-link text-purple"> More </a>
                </div>
            </div>
            <div class="manga-carousel">
                @foreach ($hotMangaUpdate as $manga)
                    <div class="col-6 col-md-4 col-lg-2 px-1 mx-2 box-shadow-mini my-3">
                        <div class="card manga mb-0">
                            <a href="#">
                                <img class="img-thumbnail card-img-top" src="/manga/{{ $manga->thumbnail }}">
                            </a>
                            <div class="card-body py-2 px-1">
                                <h5 class="card-title mb-1">{{ $manga->title }}</h5>
                                <div class="row">
                                    <div class="col-4 text-left sub-title">
                                        <i class="fa fa-star"></i>
                                        {{ $manga->rating }}
                                    </div>
                                    <div class="col-8 text-right sub-title">
                                        <i class="fa fa-eye"></i>
                                        {{ number_format($manga->views, 2) }}
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <div class="col-6 text-left">
                                        <a href="#" class="latest-chap">Ch. 20</a>
                                    </div>
                                    <div class="col-6 pl-0 text-right sub-title">
                                        2 hours ago
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="section pt-md-5">
            <div class="row">
                <div class="col-6">
                    <h2 class="position-relative pl-3">
                        <span class="purple-indicator"></span>
                        Being Read Right Now
                    </h2>
                </div>
                <div class="col-6 text-right">
                    <a href="#" class="text-link text-purple"> More </a>
                </div>
            </div>
            <div class="row py-3 manga-carousel">
                @foreach ($highestRated as $manga)
                    <div class="col-6 col-md-4 col-lg-2 px-1 mx-2 box-shadow-mini my-3">
                        <div class="card manga mb-0">
                            <a href="#">
                                <img class="img-thumbnail card-img-top" src="/manga/{{ $manga->thumbnail }}">
                            </a>
                            <div class="card-body py-2 px-1">
                                <h5 class="card-title mb-1">{{ $manga->title }}</h5>
                                <div class="row">
                                    <div class="col-4 text-left sub-title">
                                        <i class="fa fa-star"></i>
                                        {{ $manga->rating }}
                                    </div>
                                    <div class="col-8 text-right sub-title">
                                        <i class="fa fa-eye"></i>
                                        {{ number_format($manga->views, 2) }}
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <div class="col-6 text-left">
                                        <a href="#" class="latest-chap">Ch. 20</a>
                                    </div>
                                    <div class="col-6 pl-0 text-right sub-title">
                                        2 hours ago
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="section pt-md-5">
            <div class="row">
                <div class="col-6">
                    <h2 class="position-relative pl-3">
                        <span class="purple-indicator"></span>
                        Highest Rated
                    </h2>
                </div>
                <div class="col-6 text-right">
                    <a href="#" class="text-link text-purple"> More </a>
                </div>
            </div>
            <div class="row py-3 manga-carousel">
                @foreach ($highestRated as $manga)
                    <div class="col-6 col-md-4 col-lg-2 px-1 mx-2 box-shadow-mini my-3">
                        <div class="card manga mb-0">
                            <a href="#">
                                <img class="img-thumbnail card-img-top" src="/manga/{{ $manga->thumbnail }}">
                            </a>
                            <div class="card-body py-2 px-1">
                                <h5 class="card-title mb-1">{{ $manga->title }}</h5>
                                <div class="row">
                                    <div class="col-4 text-left sub-title">
                                        <i class="fa fa-star"></i>
                                        {{ $manga->rating }}
                                    </div>
                                    <div class="col-8 text-right sub-title">
                                        <i class="fa fa-eye"></i>
                                        {{ number_format($manga->views, 2) }}
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <div class="col-6 text-left">
                                        <a href="#" class="latest-chap">Ch. 20</a>
                                    </div>
                                    <div class="col-6 pl-0 text-right sub-title">
                                        2 hours ago
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        <div class="section pt-md-5">
            <h2 class="position-relative pl-3">
                <span class="purple-indicator"></span>
                Latest Updates
            </h2>
            <div class="row py-3">
                @foreach ($latestUpdates as $manga)
                    <div class="col-6 col-md-4 col-lg-2 px-1 mx-2 my-2 box-shadow-mini">
                        <div class="card manga mb-0">
                            <a href="#">
                                <img class="img-thumbnail card-img-top" src="/manga/{{ $manga->thumbnail }}">
                            </a>
                            <div class="card-body py-2 px-1">
                                <h5 class="card-title mb-1">{{ $manga->title }}</h5>
                                <div class="row">
                                    <div class="col-4 text-left sub-title">
                                        <i class="fa fa-star"></i>
                                        {{ $manga->rating }}
                                    </div>
                                    <div class="col-8 text-right sub-title">
                                        <i class="fa fa-eye"></i>
                                        {{ number_format($manga->views, 2) }}
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <div class="col-6 text-left">
                                        <a href="#" class="latest-chap">Ch. 20</a>
                                    </div>
                                    <div class="col-6 pl-0 text-right sub-title">
                                        2 hours ago
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
        </div>
        @endsection
    </div>