@extends('layouts.app')

@section('content')
    <div class="container py-4">
        <div class="d-flex" id="wrapper">
            @include('admin.layout.sidebar')
            <div class="container-fluid">
                <h1 class="mt-4">Dashboard</h1>
            </div>
        </div>
    </div>
@endsection