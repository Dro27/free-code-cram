@extends('layouts.app')

{{--

REFACTOR THIS! CREATE A MANGA DIR AND MOVE THIS ALONG WITH THE CREATE/EDIT FORM

--}}

@section('content')
    <div class="container py-4 admin-manga">
        <div class="d-flex" id="wrapper">
            @include('admin.layout.sidebar')
            <div class="container-fluid">
                <div class="row">
                    <div class="col-6">
                        <h2 class="position-relative pl-3">
                            <span class="purple-indicator"></span>
                            Manga
                        </h2>
                    </div>
                    <div class="col-6 text-right">
                        <a href="{{ route('admin.manga.create.show') }}" class="text-link text-purple"> <i class="fa fa-plus"></i> Create </a>
                    </div>
                </div>
                @if (session('is_deleted'))
                    @if (session('is_deleted') == 1)
                        <div class="alert alert-success text-center">
                            Successfully Deleted!
                        </div>
                    @endif
                @endif
                <div class="table-responsive">
                    <table class="table table-bordered table-hover">
                        <thead>
                            <tr>
                                <th scope="col">ID</th>
                                <th scope="col">Thumbnail</th>
                                <th scope="col">Title</th>
                                <th scope="col">Author</th>
                                <th scope="col">Rating</th>
                                <th scope="col">Views</th>
                                <th scope="col">Created At</th>
                                <th scope="col">Modified At</th>
                                <th scope="col">Status</th>
                                <th scope="col">Action</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($list as $manga)
                                <tr>
                                    <th scope="row">{{$manga->id}}</th>
                                    <td width="100" align="center">
                                        <img src="/manga/{{ $manga->thumbnail }}" width="50px">
                                    </td>
                                    <td>{{ $manga->title }}</td>
                                    <td>{{ $manga->author->name }}</td>
                                    <td align="center">{{ $manga->rating }}</td>
                                    <td>{{ number_format($manga->views, 2) }}</td>
                                    <td width="150">{{ date('M. j, Y', strtotime($manga->created_at)) }}</td>
                                    <td width="150">{{ date('M. j, Y', strtotime($manga->updated_at)) }}</td>
                                    <td>
                                        @if($manga->status == 0) Inactive
                                        @else Active
                                        @endif
                                    </td>
                                    <td>
                                        <a href="{{ route('admin.manga.edit.show', ['id' => $manga->id]) }}">
                                            Edit
                                        </a>
                                    </td>
                                </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-center">
                        {{ $list->links() }}
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection