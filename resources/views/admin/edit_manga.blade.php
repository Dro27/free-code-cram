@extends('layouts.app')

{{--

REFACTOR THIS EDIT AND CREATE FORM SHOULD BE THE SAME YOU FOOL

--}}

@section('content')
    <div class="container profile py-4">
        <img src="/images/yuigahama_bg.png" class="position-absolute bg-image">
        <div class="col-12 col-lg-10 mx-auto rounded box-shadow-mini p-0">
            <div class="card">
                <div class="card-header">
                    <h3 class="mb-0"> Edit Manga</h3>
                </div>
                <div class="card-body py-5">
                    <div class="col-lg-7 mx-lg-auto">
                        @if (session('status'))
                            @if (session('status') == 1)
                                <div class="alert alert-success text-center">
                                    Successfully Updated!
                                </div>
                            @endif
                            @if (session('status') == 2)
                                <div class="alert alert-danger text-center">
                                    Something went wrong!
                                </div>
                            @endif
                        @endif
                        <form method="POST" enctype="multipart/form-data" action="{{ route('admin.manga.edit', ['id' => $data[0]->id]) }}"> @csrf
                            <div class="form-group row">
                                <label for="title" class="col-md-3 col-form-label">{{ __('Title') }}</label>
                                <div class="col-md-9">
                                    <input id="title" type="text" class="form-control @error('title') is-invalid @enderror" name="title" value="{{ $data[0]->title }}" required autocomplete="title" autofocus>
                                    @error('title')
                                    <span class="invalid-feedback" role="alert">
                                        <strong>{{ $message }}</strong>
                                    </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="author" class="col-md-3 col-form-label">{{ __('Title') }}</label>
                                <div class="col-md-9">
                                    <select id="author" name="author" class="form-control">
                                        @foreach ($authors as $author)
                                            <option value="{{$author->id}}" @if($author->id == $data[0]->author->id)  selected @endif>{{$author->name}}</option>
                                        @endforeach
                                    </select>
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="upload-image" class="col-md-3 col-form-label">Example file input</label>
                                <div class="col-md-9">
                                    <input type="file" class="form-control form-control-file" name="image" id="upload-image">
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-7 mx-auto mt-4 mb-2">
                                    <button type="submit" class="btn btn-block btn-submit">
                                        {{ __('SUBMIT') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                        <form action="{{ route('admin.manga.delete',  ['id' => $data[0]->id]) }}" method="POST">
                            @method('DELETE') @csrf
                            <div class="form-group row mb-0">
                                <div class="col-md-7 mx-auto">
                                    <button type="submit" class="btn btn-block btn-danger">
                                        <i class="fa fa-trash"></i>
                                        {{ __('DELETE') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection