@extends('layouts.app')

@section('content')
    <div class="container profile py-4">
        <img src="/images/yuigahama_bg.png" class="position-absolute bg-image">
        <div class="col-12 col-lg-10 mx-auto rounded box-shadow-mini p-0">
            <div class="card">
                <div class="card-header">
                    <h3 class="mb-0"> Glad your here...! Baga... </h3>
                </div>
                <div class="card-body py-5">
                    <div class="col-lg-7 mx-lg-auto">
                        @if (session('status'))
                            @if (session('status') == 1)
                                <div class="alert alert-danger text-center">
                                    Incorrect Password!
                                </div>
                            @endif
                            @if (session('status') == 2)
                                <div class="alert alert-warning text-center">
                                    Password don't match!
                                </div>
                            @endif
                                @if (session('status') == 3)
                                    <div class="alert alert-success text-center">
                                        Profile Updated!
                                    </div>
                                @endif
                        @endif
                        <form method="POST" action="{{ route('profile.put', ['id' => Auth::user()->id]) }}"> @csrf
                            <div class="form-group row">
                                <label for="name" class="col-md-3 col-form-label">{{ __('Name') }}</label>
                                <div class="col-md-9">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror" name="name" value="{{ Auth::user()->name }}" required autocomplete="name" autofocus>
                                    @error('name')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="email" class="col-md-3 col-form-label">{{ __('E-Mail Address') }}</label>
                                <div class="col-md-9">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ Auth::user()->email }}" required autocomplete="email">
                                    @error('email')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password" class="col-md-3 col-form-label">{{ __('Password') }}</label>
                                <div class="col-md-9">
                                    <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="new-password">
                                    @error('password')
                                    <span class="invalid-feedback" role="alert">
                                <strong>{{ $message }}</strong>
                            </span>
                                    @enderror
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password-confirm" class="col-md-3 col-form-label">{{ __('Confirm Password') }}</label>
                                <div class="col-md-9">
                                    <input id="password-confirm" type="password" class="form-control" name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="password-current" class="col-md-3 col-form-label">{{ __('Current Password') }}</label>
                                <div class="col-md-9">
                                    <input id="password-current" type="password" class="form-control" name="password_current" required autocomplete="new-password">
                                </div>
                            </div>
                            <div class="form-group row mb-0">
                                <div class="col-md-7 mx-auto my-4">
                                    <button type="submit" class="btn btn-block btn-submit">
                                        {{ __('UPDATE MY INFO') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection