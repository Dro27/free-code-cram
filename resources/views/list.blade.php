@extends('layouts.app')

@section('content')
    <div class="container py-4">
        <div class="section pt-md-5">
            <h2 class="position-relative pl-3">
                <span class="purple-indicator"></span>
                {{ $title }}
            </h2>
            <div class="row py-3">
                @foreach ($list as $manga)
                    <div class="col-6 col-md-4 col-lg-2 px-1 mx-2 my-2 box-shadow-mini">
                        <div class="card manga mb-0">
                            <a href="#">
                                <img class="img-thumbnail card-img-top" src="/manga/{{ $manga->thumbnail }}">
                            </a>
                            <div class="card-body py-2 px-1">
                                <h5 class="card-title mb-1">{{ $manga->title }}</h5>
                                <div class="row">
                                    <div class="col-4 text-left sub-title">
                                        <i class="fa fa-star"></i>
                                        {{ $manga->rating }}
                                    </div>
                                    <div class="col-8 text-right sub-title">
                                        <i class="fa fa-eye"></i>
                                        {{ number_format($manga->views, 2) }}
                                    </div>
                                </div>
                                <div class="row align-items-center">
                                    <div class="col-6 text-left">
                                        <a href="#" class="latest-chap">Ch. 20</a>
                                    </div>
                                    <div class="col-6 pl-0 text-right sub-title">
                                        2 hours ago
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                @endforeach
            </div>
            <div class="d-flex justify-content-center mt-4">
                {{ $list->links() }}
            </div>
        </div>
        @endsection
    </div>