@extends('layouts.app')

@section('content')
    <div class="container profile py-4">
        <img src="/images/yuigahama_bg.png" class="position-absolute bg-image">
        <div class="col-12 col-lg-10 mx-auto rounded box-shadow-mini p-0">
            <div class="card">
                <div class="card-header">
                    <h3 class="mb-0"> Reset Password </h3>
                </div>
                <div class="card-body py-5">
                    <div class="col-lg-7 mx-lg-auto">
                        @if (session('status'))
                            <div class="alert alert-success" role="alert">
                                {{ session('status') }}
                            </div>
                        @endif
                        <form method="POST" action="{{ route('password.email') }}">
                            @csrf

                            <div class="form-group row">
                                <label for="email" class="col-md-4 col-form-label text-md-right">{{ __('E-Mail Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="form-group row mb-0 mt-5">
                                <div class="col-md-7 mx-auto">
                                    <button type="submit" class="btn btn-block btn-submit">
                                        {{ __('Send Password Reset Link') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
