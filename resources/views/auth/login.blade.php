@extends('layouts.app')

@section('content')
<div class="container login py-4">
    <img src="/images/login-img.png" class="position-absolute bg-image">
    <div class="col-12 col-lg-10 mx-auto rounded box-shadow-mini p-0">
        <div class="card">
            <div class="card-header">
                <h3 class="mb-0"> LOGIN </h3>
            </div>
            <div class="card-body py-5">
                <div class="col-lg-7 mx-lg-auto">
                    <form method="POST" action="{{ route('login') }}"> @csrf
                        <div class="form-group row">
                            <label for="email" class="col-md-3 col-form-label">{{ __('E-Mail Address') }}</label>
                            <div class="col-md-9">
                                <input id="email" type="email" class="form-control @error('email') is-invalid @enderror" name="email" value="{{ old('email') }}" required autocomplete="email" autofocus>
                                @error('email')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <label for="password" class="col-md-3 col-form-label">{{ __('Password') }}</label>
                            <div class="col-md-9">
                                <input id="password" type="password" class="form-control @error('password') is-invalid @enderror" name="password" required autocomplete="current-password">
                                @error('password')
                                <span class="invalid-feedback" role="alert">
                                    <strong>{{ $message }}</strong>
                                </span>
                                @enderror
                            </div>
                        </div>
                        <div class="form-group col-12 m-0 p-0 text-right">
                            @if (Route::has('password.request'))
                                <a class="text-link text-danger forgot-password" href="{{ route('password.request') }}">
                                    {{ __('Forgot Your Password?') }}
                                </a>
                            @endif
                        </div>
                        <div class="form-group row py-4 mb-0">
                            <div class="col-md-12 text-center">
                                <div class="form-check">
                                    <input class="form-check-input" type="checkbox" name="remember" id="remember" {{ old('remember') ? 'checked' : '' }}>
                                    <label class="form-check-label" for="remember">
                                        {{ __('Keep me login in this computer') }}
                                    </label>
                                </div>
                            </div>
                        </div>
                        <div class="form-group row mb-0">
                            <div class="col-md-7 mx-auto my-4">
                                <button type="submit" class="btn btn-block btn-submit">
                                    {{ __('LOGIN TO MY ACCOUNT') }}
                                </button>
                                <div class="py-3">
                                    <p class="text-center sub-text mb-0">
                                        Don't have an account? Sign up <a href="text-link"> here. </a>
                                    </p>
                                    <p class="text-center sub-text mb-0">
                                        This site is protected by reCAPTCHA and the Google.
                                        <a href="https://policies.google.com/privacy">Privacy Policy</a> and
                                        <a href="https://policies.google.com/terms">Terms of Service</a> apply.
                                    </p>
                                </div>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection
