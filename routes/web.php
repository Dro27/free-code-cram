<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

Auth::routes();

// Homepage
Route::get('/', 'HomeController@index')->name('homepage.show');

// Profile
Route::get('/profile/{id}', 'ProfileController@index')->name('profile.show');
Route::post('/profile/{id}/update', 'ProfileController@update')->name('profile.put');

// Manga
Route::get('/manga/{title}', 'MangasController@index')->name('manga.show');

/**
 * THIS IS TEMPORARY
 * REFACTOR THIS SHIT ITS POSSIBLE TO USE JUST ONE CONTROLLER FOR BROWSING MANGA
 */
Route::get('/latest-updates', 'MangasController@latest')->name('manga.latest.list.show');
Route::get('/hot', 'MangasController@hot')->name('manga.hot.list.show');

// Admin Login
Route::get('/admin/login', 'AdminLoginController@index')->name('admin.login.show');
Route::group(['middleware'  => ['auth','admin']], function() {
    Route::get('/admin/dashboard', 'Admin\AdminDashboardController@index')->name('admin.dashboard.show');
    Route::get('/admin/manga', 'Admin\AdminMangaController@index')->name('admin.manga.show');
    Route::get('/admin/manga/create', 'Admin\AdminMangaController@create')->name('admin.manga.create.show');
    Route::get('/admin/manga/{id}/edit', 'Admin\AdminMangaController@edit')->name('admin.manga.edit.show');
    Route::post('/admin/manga/new', 'Admin\AdminMangaController@triggerCreate')->name('admin.manga.create');
    Route::post('/admin/manga/{id}/update', 'Admin\AdminMangaController@triggerUpdate')->name('admin.manga.edit');
    Route::delete('/admin/manga/{id}/delete', 'Admin\AdminMangaController@delete')->name('admin.manga.delete');
});